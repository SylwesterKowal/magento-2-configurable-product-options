<?php
declare(strict_types=1);

namespace Kowal\ConfigurableProductOptions\Plugin\Frontend\Magento\ConfigurableProduct\Model\ResourceModel\Attribute;

use Magento\CatalogInventory\Model\ResourceModel\Stock\Status;
use Magento\Framework\DB\Select;

class OptionSelectBuilderInterface
{

    /**
     * CatalogInventory Stock Status Resource Model.
     *
     * @var Status
     */
    private $stockStatusResource;

    /**
     * @param Status $stockStatusResource
     */
    public function __construct(Status $stockStatusResource)
    {
        $this->stockStatusResource = $stockStatusResource;
    }

    public function afterGetSelect(
        \Magento\ConfigurableProduct\Model\ResourceModel\Attribute\OptionSelectBuilderInterface $subject,
        Select $result
    ) {
        $result->joinInner(
            ['stock' => $this->stockStatusResource->getMainTable()],
            'stock.product_id = entity.entity_id',
            ['stock.stock_status']
        );
        return $result;
    }
}

