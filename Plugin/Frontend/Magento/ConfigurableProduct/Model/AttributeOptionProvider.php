<?php
declare(strict_types=1);

namespace Kowal\ConfigurableProductOptions\Plugin\Frontend\Magento\ConfigurableProduct\Model;

class AttributeOptionProvider
{

    public function afterGetAttributeOptions(
        \Magento\ConfigurableProduct\Model\AttributeOptionProvider $subject,
        $result
    ) {
        foreach ($result as &$option) {

            if(isset($option['stock_status']) && $option['stock_status']==0){
                $option['option_title']  = $option['option_title'].__(' - Out of stock');
            }
        }
        return $result;
    }
}

