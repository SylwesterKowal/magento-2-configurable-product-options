# Mage2 Module Kowal ConfigurableProductOptions

    ``kowal/module-configurableproductoptions``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Dodatkowy opis nazwy wariantu

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_ConfigurableProductOptions`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-configurableproductoptions`
 - enable the module by running `php bin/magento module:enable Kowal_ConfigurableProductOptions`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration




## Specifications

 - Plugin
	- afterGetSelect - Magento\ConfigurableProduct\Model\ResourceModel\Attribute\OptionSelectBuilderInterface > Kowal\ConfigurableProductOptions\Plugin\Frontend\Magento\ConfigurableProduct\Model\ResourceModel\Attribute\OptionSelectBuilderInterface

 - Plugin
	- afterGetAttributeOptions - Magento\ConfigurableProduct\Model\AttributeOptionProvider > Kowal\ConfigurableProductOptions\Plugin\Frontend\Magento\ConfigurableProduct\Model\AttributeOptionProvider


## Attributes



